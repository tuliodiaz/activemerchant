module ActiveMerchant #:nodoc:
  module Billing #:nodoc:
    class PayfirmaGateway < Gateway

      class_attribute :auth_test_url, :auth_live_url, :services

      #REST services for customer, plan and transaction operations
      self.test_url = 'https://sandbox-apigateway.payfirma.com'
      self.live_url = 'https://apigateway.payfirma.com'

      #Auth services for token creation and renovation
      #token expires every 20 minutes
      self.auth_test_url = 'https://sandbox-auth.payfirma.com'
      self.auth_live_url = 'https://auth.payfirma.com'

      self.services = {
          :authorization => 'oauth',
          :customer      => 'customer-service',
          :plan          => 'plan-service',
          :transaction   => 'transaction-service'
      }

      self.supported_countries = %w(US CA)
      self.default_currency = 'CAD'
      self.money_format = :dollars
      self.supported_cardtypes = [:visa, :master, :american_express, :discover]

      self.homepage_url = 'https://www.payfirma.com'
      self.display_name = 'Payfirma'


      # Initialize the Gateway
      #
      # The gateway requires that a valid client_id and client_secret be passed
      # in the +options+ hash. Likewise the login and password combination OR
      # a refresh token from a previous grant call.
      #
      # ==== Options
      #
      # * <tt>:client_id</tt> -- Client ID (Required)
      # * <tt>:client_secret</tt> -- client_secret (Required)
      # * <tt>:login</tt> -- Username or email (Optional)
      # * <tt>:password</tt> -- Password (Optional)
      # * <tt>:refresh_token</tt> -- Refresh token (Optional)
      def initialize(options={})
        requires!(options, :client_id, :client_secret)
        super
      end

      def purchase(money, payment, options={})
        transaction('sale', money, payment, options)
      end

      def authorize(money, payment, options={})
        transaction('authorize', money, payment, options)
      end

      def store(credit_card, options={})
        #Client has many cards
        #Credit card cannot be saved without a client

        ##
        #Store customer if customer_lookup_id is unknown
        ##
        unless options[:customer_lookup_id]
          post = {}
          add_customer_data(post, credit_card, options)
          add_billing_address(post, options)
          r = commit(:customer, :post, 'customer', post)
          if r.success?
            options[:customer_lookup_id] = r.params['lookup_id']
          else
            return r
          end
        end

        ##
        #Store Credit Card
        ##
        post = {}
        add_default(post, options)
        add_credit_card(post, credit_card)
        commit(:customer, :post, "customer/#{CGI.escape(options[:customer_lookup_id])}/card", post)
      end

      def unstore(token)
        customer_id, customer_lookup_id, card_id, card_lookup_id = token.split('|')
        if card_lookup_id
          #delete credit card
          commit(:customer, :delete, "customer/#{CGI.escape(customer_lookup_id)}/card/#{CGI.escape(card_lookup_id)}", nil)
        else
          #delete customer
          commit(:customer, :delete, "customer/#{CGI.escape(customer_lookup_id)}", nil)
        end
      end

      def capture(money, authorization, options={})
        post = {}
        add_invoice(post, money, options)

        commit(:transaction, :post, "capture/#{authorization}", post)
      end

      def refund(money, authorization, options={})
        post = {}
        add_invoice(post, money, options)

        commit(:transaction, :post, "refund/#{authorization}", post)
      end

      ####
      ## Not support yet for void transaction
      ## If you’d like to release an Authorization,
      ## you could perform a Capture for $0.01 on
      ## the original Authorization and then
      ## Refund that $0.01 Capture amount.
      ####
      def void(authorization, options={})
        r = capture(1,authorization, options)
        refund(1, r.authorization, options) if  r.success?
      end

      ####
      ## Operation: [sale or authorize]
      ## Money: transaction amount in cents 100 = 1.00
      ## Payment: [credit_card or token]
      ## options: customer, currency, invoice_id, sending_receipt
      ####
      def transaction(operation, money, payment, options={})
        post = {}
        add_invoice(post, money, options)
        if payment.is_a?(CreditCard)
          add_credit_card(post, payment)
          add_customer_data(post, payment, options)
          add_billing_address(post, options)
          commit(:transaction, :post, operation, post)
        else
          customer_id, customer_lookup_id, card_id, card_lookup_id = payment.split('|')
          if card_lookup_id
            commit(:transaction, :post, "#{operation}/customer/#{customer_lookup_id}/card/#{card_lookup_id}", post)
          else
            commit(:transaction, :post, "#{operation}/customer/#{customer_lookup_id}", post)
          end
        end
      end

      def password_grant(login, password)
        post = {}
        add_grant_type(post, :password)
        add_user_credentials(post, login, password)
        commit(:authorization, :post, 'token', post)
      end

      def client_credential_grant(client_id, client_secret)
        post = {}
        add_grant_type(post, :client_credentials)
        add_api_credentials(post, client_id, client_secret)
        commit(:authorization, :post, 'token', post)
      end

      def refresh_token_grant(refresh_token)
        post = {}
        add_grant_type(post, :refresh_token)
        add_refresh_token(post, refresh_token)
        add_api_credentials(post, @options[:client_id], @options[:client_secret])
        commit(:authorization, :post, 'token', post, false)
      end

      def supports_scrubbing?
        true
      end

      def scrub(transcript)
        transcript
      end

      def commit(service, method, action, parameters, include_auth_header=true)
        @options[:logger].info '-------Payfirma start commit------'
        if service == :authorization
          parameters = uri_format(parameters)
        else
          add_test_mode(parameters) if test? && service == :transaction
          parameters = json_format(parameters)
        end
        response = begin
          retrive_or_refresh_bearer! if service != :authorization
          @options[:logger].info "method: #{method}"
          @options[:logger].info "url: #{get_base_url(service) +  action}"
          @options[:logger].info "parameters: #{parameters.to_yaml}"
          @options[:logger].info "headers: #{headers(service, include_auth_header)}"
          parse(ssl_request(method, get_base_url(service) +  action, parameters, headers(service, include_auth_header)))
        rescue ResponseError => e
          @options[:logger].info "Exception: #{e.to_yaml}"
          return Response.new(false, 'Invalid Login') if(e.response.code == '401')
          parse(e.response.body)
        end

        @options[:logger].info "Response: #{response.to_yaml}"
        @options[:logger].info '-------Payfirma End commit------'

        ActiveMerchant::Billing::Response.new(
            success_from(response),
            message_from(response),
            response,
            authorization: authorization_from(response),
            test: test?)
      end


      private

      def add_credit_card(post, credit_card)
        post[:card_expiry_month]= format(credit_card.month, :two_digits)
        post[:card_expiry_year] = format(credit_card.year, :two_digits)
        post[:card_number]      = credit_card.number
        post[:cvv2]             = credit_card.verification_value if credit_card.verification_value?
      end

      def add_invoice(post, money, options)
        post[:amount]         = amount(money)
        post[:currency]       = (options[:currency] || currency(money))
        post[:invoice_id]     = options[:invoice_id] if options.has_key?(:invoice_id)
        post[:sending_receipt]= options[:sending_receipt] if options.has_key?(:sending_receipt)
      end

      def add_customer_data(post, credit_card, options)
        post[:first_name] = credit_card.first_name if credit_card.first_name?
        post[:last_name]  = credit_card.last_name  if credit_card.last_name?
        post[:email]      = options[:email] || custom_email
        post[:custom_id]  = options[:custom_id] if options.has_key?(:custom_id)
      end

      def add_billing_address(post, options)
        billing_address = options[:billing_address] || options[:address]
        post[:address1]   = billing_address[:address1]
        post[:address2]   = billing_address[:address2]
        post[:city]       = billing_address[:city]
        post[:country]    = billing_address[:country]
        post[:province]   = billing_address[:state]
        post[:postal_code]= billing_address[:zip]
        post[:company]    = billing_address[:company]    if billing_address.has_key?(:company)
        post[:bcc_emails] = billing_address[:bcc_emails] if billing_address.has_key?(:bcc_emails)
        post[:telephone]  = billing_address[:telephone]  if billing_address.has_key?(:telephone)
      end

      def add_default(post, options)
        post[:is_default] = true if options[:is_default]
      end


      def add_user_credentials(post, login, password)
        post[:username] = login
        post[:password] = password
      end


      def add_api_credentials(post, client_id, client_secret)
        post[:client_id] = client_id
        post[:client_secret] = client_secret
      end


      def add_grant_type(post, type)
        post[:grant_type] = type
      end


      def add_refresh_token(post, refresh_token)
        post[:refresh_token] = refresh_token
      end


      def add_test_mode(post)
        post[:test_mode] = true
      end

      def parse(body)
        body.blank? ? {} : JSON.parse(body)
      end

      def success_from(response)
        if response.key?('transaction_success')
          response['transaction_success']
        elsif response.key?('errors')
          false
        else
          true
        end
      end

      def message_from(response)
        if response['transaction_result']
          "#{response['transaction_result']} #{response['transaction_message']}".strip
        elsif response['errors']
          response['errors'].map{|e| "#{e['code']}: #{e['message']}"}.join(';')
        else
          'SUCCESS'
        end
      end

      def authorization_from(response)
        if response.has_key?('id')
          if response.has_key?('cards')
            [response['id'], response['lookup_id'], response['cards'].last['id'], response['cards'].last['lookup_id']].join('|')
          else
            response['id']
          end
        end
      end

      def uri_format(parameters)
        parameters.collect { |key, value| "#{key}=#{CGI.escape(value.to_s)}" }.join('&') if parameters
      end

      def json_format(parameters)
        parameters.to_json if parameters
      end

      def get_base_url(service)
        url = begin
          if service == :authorization
            test? ? self.auth_test_url : self.auth_live_url
          else
            test? ? self.test_url : self.live_url
          end
        end
        "#{url}/#{self.services[service]}/"
      end

      def headers(service, include_auth_header)
        response = {}
        if service == :authorization
          response['Content-Type']  = 'application/x-www-form-urlencoded'
          response['Authorization'] = "Basic #{basic_token}" if include_auth_header
        else
          response['Content-Type']  = 'application/json'
          response['Authorization'] = "Bearer #{bearer_token}" if include_auth_header
        end
        response
      end

      def basic_token
        Base64.strict_encode64("#{@options[:client_id]}:#{@options[:client_secret]}")
      end

      def refresh_token=(token)
        @options[:refresh_token] = token
      end
      def refresh_token
        @options[:refresh_token]
      end
      def bearer_token=(token)
        @options[:bearer_token] = token
      end
      def bearer_token
        @options[:bearer_token]
      end
      def bearer_token_expiry=(expires_in)
        @options[:expires_in] = Time.now + expires_in.to_i.seconds
      end
      def bearer_token_expiry
        @options[:expires_in]
      end

      def retrive_or_refresh_bearer!
=begin
        response = begin
          if refresh_token
            refresh_token_grant refresh_token if !bearer_token_expiry || Time.now > bearer_token_expiry
          elsif @options[:login] && @options[:password]
            password_grant(@options[:login], @options[:password])
          end
        end
=end

        response = begin
          if !bearer_token_expiry || Time.now > bearer_token_expiry
            #Deprecated password_grant
            #password_grant(@options[:login], @options[:password])
            client_credential_grant(@options[:client_id], @options[:client_secret])
          end
        end

        if response && response.success?
          self.bearer_token        = response.params['access_token']
          self.refresh_token       = response.params['refresh_token']
          self.bearer_token_expiry = response.params['expires_in']
        end
        response
      end

      def custom_email
        id = options[:custom_id] || SecureRandom.hex(8)
        "#{id}@myhost.com"
      end

    end

  end
end

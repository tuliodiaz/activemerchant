require 'test_helper'

class RemotePayfirmaTest < Test::Unit::TestCase
  def setup
    @gateway = PayfirmaGateway.new(fixtures(:payfirma))

    @odd_amount = 5126
    @even_amount = 4011

    @credit_card = credit_card('4111111111111111', month: 06, year: 2017, verification_value: 123)
    customer = {
        :email      => 'noname@gmail.com',
        :first_name => 'peter',
        :last_name  => 'pan',
        :company    => 'MyCompany',
        :bcc_emails => 'tulio_diaz126@hotmail.com',
        :telephone  => '5149195555',
        :address1   => '7351 rue chambord',
        :address2   => 'apt 2',
        :city       => 'Montreal',
        :province   => 'QC',
        :country    => 'Canada',
        :postal_code=> 'h2e1w9',
        :custom_id  => '85695'
    }
    @options = {
        :customer => customer
    }
  end

  def test_successful_purchase
    response = @gateway.purchase(@odd_amount, @credit_card, @options)
    assert_success response
    assert_equal 'APPROVED', response.message
  end

=begin
  def test_successful_purchase
    response = @gateway.purchase(@odd_amount, @credit_card, @options)
    assert_success response
    assert_equal 'APPROVED', response.message
  end

  def test_unsuccessful_purchase
    response = @gateway.purchase(@even_amount, @credit_card, @options)
    assert_failure response
    assert_equal 'DECLINED', response.message
  end

  def test_successful_authorization
    response = @gateway.authorize(@odd_amount, @credit_card, @options)
    assert_success response
    assert_equal 'APPROVED', response.message
  end

  def test_unsuccessful_authorization
    response = @gateway.authorize(@even_amount, @credit_card, @options)
    assert_failure response
    assert_equal 'DECLINED', response.message
  end

  def test_successful_store
    @options[:customer][:email] = "#{SecureRandom.hex}@myhost.com"
    assert response = @gateway.store(@credit_card,@options)
    assert_success response
    assert_equal 'SUCCESS', response.message
  end

  def test_successful_unstore
    @options[:customer][:email] = "#{SecureRandom.hex}@myhost.com"
    assert response = @gateway.store(@credit_card,@options)
    assert_success response
    assert_equal 'SUCCESS', response.message
    customer_lookup_id    = response.params['lookup_id']
    credit_card_lookup_id = response.params['cards'][0]['lookup_id']
    assert response = @gateway.unstore(customer_lookup_id,credit_card_lookup_id)
    assert_success response
    assert_equal 'SUCCESS', response.message
  end

  def test_successful_purchase_with_customer
    @options[:customer][:email] = "#{SecureRandom.hex}@myhost.com"
    assert response = @gateway.store(@credit_card,@options)
    assert_success response
    assert_equal 'SUCCESS', response.message
    customer_id = response.params['id'].to_s
    response = @gateway.purchase(@odd_amount, customer_id, @options)
    assert_success response
    assert_equal 'APPROVED', response.message
  end

  def test_successful_purchase_with_customer_and_credit_card
    @options[:customer][:email] = "#{SecureRandom.hex}@myhost.com"
    assert response = @gateway.store(@credit_card,@options)
    assert_success response
    assert_equal 'SUCCESS', response.message
    customer_id    = response.params['id'].to_s
    card_lookup_id = response.params['cards'][0]['lookup_id'].to_s
    response = @gateway.purchase(@odd_amount, {customer_id: customer_id, card_lookup_id: card_lookup_id}, @options)
    assert_success response
    assert_equal 'APPROVED', response.message
  end

  def test_successful_authorize_with_customer
    @options[:customer][:email] = "#{SecureRandom.hex}@myhost.com"
    assert response = @gateway.store(@credit_card,@options)
    assert_success response
    assert_equal 'SUCCESS', response.message
    customer_id  = response.params['id'].to_s
    response = @gateway.authorize(@odd_amount, customer_id, @options)
    assert_success response
    assert_equal 'APPROVED', response.message
  end

  def test_successful_authorize_with_customer_and_credit_card
    @options[:customer][:email] = "#{SecureRandom.hex}@myhost.com"
    assert response = @gateway.store(@credit_card,@options)
    assert_success response
    assert_equal 'SUCCESS', response.message
    customer_id    = response.params['id'].to_s
    card_lookup_id = response.params['cards'][0]['lookup_id'].to_s
    response = @gateway.authorize(@odd_amount, {customer_id: customer_id, card_lookup_id: card_lookup_id}, @options)
    assert_success response
    assert_equal 'APPROVED', response.message
  end
=end

end
